import React from 'react';
import Parse from "./services/Apiservice"
import DropFileInput from './components/drop-file-input/DropFileInput';
import './App.css';
function App() {

    Parse.User.logIn("hello", "123")
    .then(async (res) => {
      console.log(Parse.User.current())
    })
    .catch((error) => {
      console.log(error.message);
      })
    const onFileChange = (files) => {
        var parseFile = new Parse.File(files[0].name, files[0]);
        parseFile.save().then(function(ok) {
            console.log(ok);
        }, function(error) {
            console.log(error);
        });
        var newStore = new Parse.Object("File");
        newStore.set("File", parseFile);
        newStore.save();
    }
    return (
        <div className="box">
            <h2 className="header">
                React drop files input
            </h2>
            <DropFileInput
                onFileChange={(files) => onFileChange(files)}
            />
        </div>
    );
}

export default App